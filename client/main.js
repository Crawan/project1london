Router.configure({
	layoutTemplate: 'ApplicationLayout'
});

Router.route('/', function () {
	this.render('welcome',{to:'main'});

});

Router.route('/contact', function () {
	this.render('navbar', {
		to:
			'navbar'
	});
	this.render('contact', {
		to:
			'main'
	});
});

Router.route('/places', function () {
	this.render('navbar', {
		to:
			'navbar'
	});
	this.render('places', {
		to:
			'main'
	});
});

Router.route('/tenerife', function () {
	this.render('navbar', {
		to:
			'navbar'
	});
	this.render('tenerife', {
		to:
			'main'
	});
});

Router.route('/images', function () {
	this.render('navbar', {
		to:
			'navbar'
	});


	this.render('images', {
		to: 'main'
	});


});

Router.route('/image/:_id', function () {
	this.render('navbar', {
		to:
			'navbar'
	});


	this.render('image', {
		to: 'main',
		data: function () {
			return Images.findOne({ _id:this.params._id })
        }
	});


});



/*Scroll event
Session.set('imageLimit', 8);
$(window).scroll(function (event) {
	console.log(new Date());
});*/


Template.images.helpers({
	images:
		Images.find({}, { sort: { createdOn: -1, rating: -1 } })
});


Template.places.helpers({
	images:
		Images.find()
});

Template.images.events({
	
	'click .js-del-image': function (event) {
		var image_id = this._id;
		$('#' + image_id).hide("slow", function () {
			Images.remove({ '_id': image_id });
		});
		
	},
	'click .js-rating': function (event) {
		var rating = $(event.currentTarget).data('userrating');
		console.log(rating);
		var image_id = this.id;
		console.log(image_id);
		Images.update({ _id: image_id }, { $set: {rating: rating } });
	},
	'submit .js-add_image': function (event) {
		var img_src, img_alt;
		img_src = event.target.img_src.value;
		img_alt = event.target.img_alt.value;
		console.log('src ' + img_src + ' and ' + img_alt);
		Images.insert({
			img_src: img_src,
			img_alt: img_alt,
			createdOn: new Date()
		});
		
	},
	'click .js-show-image-form': function (event) {
	
		$("#image_add_form").modal('show');
		return false;
    }
});

